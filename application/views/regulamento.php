<?php if(!$this->input->is_ajax_request()): ?>
<div class="main main-regulamento">
	<div class="centro">
<?php endif; ?>
		<div class="box-branco">
			<p>1) Promoção válida em todo território nacional, no período de 01/10/2013 à 16/12/2013, realizada pela CLP Prestação de Serviçoes Administrativos Sociedade Simples Ltda. (Mandatária), com sede na Rua Silvestre Vasconcelos Calmon, 190 – Cj. 604 – Guarulhos/SP, inscrita no CNPJ nº 02.780.640/0001-97 e Real Comercial Ltda. (Aderente), com sede na Rua Silvestre Vasconcelos Calmon, 190 – Cj. 604 – Guarulhos/SP, inscrita no CNPJ nº 02.780.640/0001-97. </p>
			<p>2) Produtos em promoção: Vinho JP Chenet Cabernet – Syrah (graduação máxima de 13º graus) importado e distribuído pela Real Comercial Ltda.</p>
			<p>3) Qualquer pessoa física maior de 18 anos, residente e domiciliada na cidade de São Paulo e regiões metropolitanas, poderá participar da presente promoção, promovida pela marca La Pastina, no período de 01/10/2013 à 16/12/2013.  </p>
			<p>4) Para participar da promoção <strong>“JP Chenet na Torcida com Você”</strong>, o consumidor ao adquirir uma garrafa de vinho JP Chenet Cabernet – Syrah até 13° graus, terá direito a cupom de participação na promoção. </p>

			<p><strong>Ponto de Venda:</strong></p>
			<p>5) A cada garrafa de vinho JP Chenet Cabernet – Syrah adquirida no período da promoção o consumidor deverá apresentar a nota fiscal de compra a promotora da promoção no ponto de venda para retirar um cupom, que deverá ser preenchido com seus dados pessoais (Nome completo, CPF, RG, Data de Nascimento, Endereço completo, Cidade, Estado, CEP, telefone, e-mail, código de barras do produto e nota fiscal da compra) e responder de forma criativa a pergunta “Em 2014 qual será sua comemoração se o Brasil for campeão?”, a resposta obrigatoriamente de deverá conter a marca do vinho (JP Chenet).</p>
			<p>5.1) Após o preenchimento do(s) cupom(ns), o participante deverá colocar o(s) cupom(ns) em urna no próprio ponto de venda, conforme período de participação descrito no item 3, sendo que os cupons entregues após esta data não serão válidos para a participação no concurso.</p>
			<p>5.2) Serão invalidados os cupons que não estiverem legíveis, com dados pessoais incompletos, sem a resposta a pergunta da promoção, sem a identificação do nº. do RG e CPF e sem identificação do endereço completo e telefone. </p>

			<p><strong>Internet:</strong></p>
			<p>6) A cada garrafa de vinho JP Chenet Cabernet – Syrah adquirida no período da promoção o consumidor poderá se inscrever na promoção acessando o site www.jpchenetnatorcidacomvoce.com.br, no período de 10h00 do dia 01/10/2013 a 23h59 do dia 16/12/2013 com seus dados pessoais (Nome completo, CPF, RG, Data de Nascimento, Endereço completo, Cidade, Estado, CEP, telefone, e-mail, código de barras do produto, nota fiscal da compra) e responder de forma criativa a pergunta “Em 2014 qual será sua comemoração se o Brasil for campeão?”, a resposta obrigatoriamente de deverá conter a marca do vinho (JP Chenet).</p>
			<p>6.1) Após a inscrição, os participantes não precisarão adotar nenhum procedimento, desta forma inscrições serão analisadas pela Comissão Julgadora.<br>Obs. O participante receberá a confirmação de sua inscrição na finalização do cadastro com a seguinte mensagem “cadastro realizado com sucesso”.</p>
			<p>7) O participante poderá concorrer com quantas inscrições desejar, sendo valido apenas uma inscrição por produto, desde que preencha as condições desta promoção, podendo, no entanto, ser classificado uma única vez durante todo o período da promoção. </p>
			<p>8) O participante inscrito nesta Promoção deverá armazenar as garrafa(s) e o(s) cupom(ns)/nota(s) fiscal(is) referente(s) à aquisição dos produtos participantes adquiridos e cadastrados durante o período de participação até o término do período de divulgação do contemplado, sendo que, o contemplado deverá, após a apuração apresentar à Empresa Promotora o(s) referido(s) documento(s), dentro do período de participação correspondente, para validação da condição de contemplado, sob pena de desclassificação. Nessa hipótese, o valor integral do prêmio será recolhido aos cofres da União, como prêmio prescrito.</p>
			<p>9) Não poderão participar do concurso funcionários da CLP Prestação de Serviços Administrativos Sociedade Simples Ltda. e Real Comercial Ltda. O cumprimento desta cláusula será de responsabilidade da empresa promotora, através de consulta ao banco de dados de funcionários no momento da apuração da Comissão Julgadora.</p>
			<p>10) Não terão validade inscrições que não preencherem as condições básicas do concurso e que impossibilitarem a verificação de sua autenticidade.</p>
			<p>11) Não poderão participar da promoção, menores de 18 anos de idade.</p>
			<p>12) Os prêmios são intransferíveis e não poderão ser convertidos em dinheiro, nem ser trocados por outro(s) produto(s).</p>
			<p>13) Forma de apuração: Todas as respostas inscritas na promoção serão julgadas por uma Comissão Julgadora de reconhecida capacidade e idoneidade, cujas decisões serão soberanas e irrecorríveis e irá classificar as 20 (vinte) melhores respostas com base nos critérios de criatividade, originalidade e adequação ao tema.</p>
			<p>14) Data da apuração da comissão julgadora: 20/12/2013</p>
			<p>15) Prêmiação: Serão selecionadas as 20 melhores respostas, que serão contempladas com 01 prêmio cada.</p>
			<p>Do 1º ao 20º prêmio: 01 (uma) Televisão Slim LED 40”, marca Sandung, modelo 40f5200 com função futebol, clear motion rete 120Hz, clear view, entradas HDMI e USB, no valor unitário de R$ 1.439,00</p>
			<p>15.1) Totalizando 20 prêmios no valor total de R$ 28.780,00</p>
			<p>16) Data da divulgação do resultado: O resultado da promoção será divulgado no site www.jpchenetnatorcidacomvoce.com.br no dia 20/12/2013 a partir das 15h.</p>
			<p>17) Exibição dos prêmios: Escritório Administrativo á Avenida Presidente Wilson, 1786 e 1866 - São Paulo/SP. A empresa comprovará a propriedade dos prêmios até 8 (oito) dias antes da data marcada para realização do concurso de acordo com o Decreto 70951/72 – Art 15 – parágrafo 1º.</p>
			<p>18) Entrega dos prêmios: Escritório Administrativo á Avenida Presidente Wilson, 1786 e 1866 - São Paulo/SP ou ainda no domicilio dos contemplados até 30 dias a contar da data da apuração de acordo com o Decreto 70951/72 – Art 5º.</p>
			<p>Os prêmios distribuídos deverão ser livres e desembaraçados de qualquer ônus para os contemplados.</p>

			<p>19) Prescrição do direito aos prêmios: 180 (cento e oitenta) dias após a data de cada apuração e os prêmios ganhos e não reclamados reverterão como Renda da União de acordo com o Art. 6º do Decreto 70951/72.</p>
			<p>20) O regulamento completo da promoção estará disponibilizado na sede da empresa e no site www.jpchenetnatorcidacomvoce.com.br. Os contemplados serão avisados através de telefone e/ou telegrama e, desde já, autorizam a utilização de seu nome, imagem e som de voz, na divulgação do resultado da promoção sem qualquer ônus para a empresa promotora pelo período de até 01 ano.</p>
			<p>21) As dúvidas e controvérsias oriundas de reclamações dos consumidores participantes da promoção deverão ser preliminarmente dirimidas pelos seus respectivos organizadores e, posteriormente, submetidas à CAIXA/GEPCO.  </p>
			<p>O PROCON local, bem como os órgãos conveniados, em cada jurisdição receberão as reclamações devidamente fundamentadas, dos consumidores participantes.  </p>


			<p><strong>22) Certificado Autorização Caixa nº. 3-1516/2013 – Distribuição Gratuita</strong><p>

		</div>

		<?php if(!$this->input->is_ajax_request()): ?>
			<a href="cadastro" title="Li o regulamento e quero participar!" class="link-branco central mb10 blocky w290">Li o regulamento e quero participar!</a>
		<?php endif; ?>

<?php if(!$this->input->is_ajax_request()): ?>
	</div>
</div>
<?php endif; ?>