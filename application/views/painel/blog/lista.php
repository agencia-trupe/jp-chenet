<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?> <a  href="painel/<?=$this->router->class?>/form" class="btn btn-success">Adicionar <?=$unidade?></a>
    </h2>
  </div>  

  <div class="row">
    <div class="span12 columns">

      <?php if ($registros): ?>

        <table class="table table-striped table-bordered table-condensed">

          <thead>
            <tr>
            	<th class="yellow header headerSortDown">Categoria</th>
            	<th class="header">Título</th>
            	<th class="header">Data</th>
            	<th class="header">Texto</th>
            	<th class="red header">Mais</th>
            	<th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($registros as $key => $value): ?>
              
                <tr class="tr-row" id="row_<?=$value->id?>">
                	<td><?=$value->categoria?></td>
                	<td class="titulo"><?=$value->titulo?></td>
                  	<td><?=formataData($value->data, 'mysql2br')?></td>
                  	<td><?=word_limiter(strip_tags($value->texto), 30)?></td>
                  	<td class="crud-actions" style="width:90px; text-align:center;">
						<a href="painel/<?=$this->router->class?>/comentarios/<?=$value->id?>" class="btn mrg-d">comentários</a>
                    	<a href="painel/<?=$this->router->class?>/imagens/<?=$value->id?>" class="btn" style="width:71px;">imagens</a>
                  	</td>
                  	<td class="crud-actions" style="width:118px; text-align:center;">
                    	<a href="painel/<?=$this->router->class?>/form/<?=$value->id?>" class="btn btn-primary">editar</a>
                    	<a href="painel/<?=$this->router->class?>/excluir/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                  	</td>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

        <?php if ($paginacao): ?>          
          <div class="pagination">
            <ul>        
              <?=$paginacao?>
            </ul>
          </div>
        <?php endif ?>

      <?php else:?>

        <h2>Nenhum Post Cadastrado</h2>

      <?php endif ?>



    </div>
  </div>