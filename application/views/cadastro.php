<?php if ($this->session->flashdata('msg_erro')): ?>
	<div id="alert" class="aberto">
		<?=$this->session->flashdata('msg_erro')?>
	</div>
<?php else: ?>
	<div id="alert"></div>
<?php endif ?>

<div class="main">
	<div class="centro">

		<form method="post" action="cadastro/enviar">

			<div class="box-branco">

				<p class="olho">
					A PROMOÇÃO "JP CHENET NA TORCIDA COM VOCÊ", SORTEARÁ 20 TVS DE 42 POLEGADAS, PARA PARTICIPAR RESPONDA A PERGUNTA E CONCORRA:
				</p>

				<p class="pergunta">
					"EM 2014 QUAL SERÁ SUA COMEMORAÇÃO SE O BRASIL FOR CAMPEÃO?"
				</p>

				<textarea name="resposta" id="input-resposta" required><?=$this->session->flashdata('resposta')?></textarea>

				<div class="sobrescrito">
					<span>DADOS PESSOAIS</span>
				</div>

				<label>NOME <input type="text" name="nome" required id="input-nome" value="<?=$this->session->flashdata('nome')?>"></label>

				<label>CPF <input type="text" name="cpf" required id="input-cpf" value="<?=$this->session->flashdata('cpf')?>" maxlength="15"></label>

				<label>RG <input type="text" name="rg" required id="input-rg" value="<?=$this->session->flashdata('rg')?>"></label>

				<label class="ml20"><span>DATA DE <br>NASCIMENTO</span> <input type="text" name="data_nascimento" required id="input-data_nascimento" value="<?=$this->session->flashdata('data_nascimento')?>" maxlength="10"></label>

				<label>ENDEREÇO <input type="text" name="endereco" required id="input-endereco" value="<?=$this->session->flashdata('endereco')?>"></label>

				<label class="ml10">CIDADE <input type="text" name="cidade" required id="input-cidade" value="<?=$this->session->flashdata('cidade')?>"></label>

				<label>ESTADO <input type="text" name="estado" required id="input-estado" value="<?=$this->session->flashdata('estado')?>" maxlength="2"></label>

				<label class="ml20">CEP <input type="text" name="cep" required id="input-cep" value="<?=$this->session->flashdata('cep')?>" maxlength="10"></label>

				<label class="ml20">TELEFONE <input type="text" name="telefone" required id="input-telefone" value="<?=$this->session->flashdata('telefone')?>" maxlength="17"></label>

				<label>E-MAIL <input type="text" name="email" required id="input-email" value="<?=$this->session->flashdata('email')?>"></label>

				<div class="sobrescrito">
					<span>DADOS DO PRODUTO</span>
				</div>

				<label>N&ordm; DO CUPOM FISCAL <input type="text" name="cupom_fiscal" required id="input-cupom_fiscal" value="<?=$this->session->flashdata('cupom_fiscal')?>"></label>

				<label>CÓDIGO DE BARRAS DO PRODUTO <input type="text" name="codigo_barra" required id="input-codigo_barra" value="<?=$this->session->flashdata('codigo_barra')?>"></label>
			</div>

			<label class="ps"><input type="checkbox" required value="1" name="aceite_regulamento" id="aceite_regulamento" <?if($this->session->flashdata('aceite_regulamento') == 1)echo "checked"?>> Li e aceito o <a href="regulamento" title="Ver o regulamento" class="shadow">regulamento</a>.</label>

			<label class="ps"><input type="checkbox" value="1" name="receber_newsletter" id="receber_newsletter" <?if($this->session->flashdata('receber_newsletter') == 1)echo "checked"?>> Quero receber novidades e promoções de JP Chenet.</label>

			<input type="submit" value="ENVIAR">

		</form>

	</div>
</div>