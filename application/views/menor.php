<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>J.P. Chenet - Na torcida com você</title>
  <meta name="description" content="Participe e concorra a 20TVs de LED de 40 polegadas para assistir aos jogos do mundial de 2014 com muito mais emoção!">
  <meta name="keywords" content="" />
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2013 Trupe Design" />

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <meta property="og:title" content="J.P. Chenet - Na torcida com você"/>
  <meta property="og:site_name" content="J.P. Chenet"/>
  <meta property="og:type" content="website"/>
  <meta property="og:image" content="<?=base_url('_imgs/layout/fb.jpg')?>"/>
  <meta property="og:url" content="<?=base_url()?>"/>
  <meta property="og:description" content="Participe e concorra a 20TVs de LED de 40 polegadas para assistir aos jogos do mundial de 2014 com muito mais emoção!"/>

  <base href="<?= base_url() ?>">
  <script> var BASE = '<?= base_url() ?>'</script>

  <?CSS(array('reset', 'base', 'fontface/stylesheet'))?>


  <?if(ENVIRONMENT == 'development'):?>

    <?JS(array('modernizr-2.0.6.min', 'less-1.3.0.min', 'jquery-1.8.0.min', $this->router->class))?>

  <?else:?>

    <?JS(array('modernizr-2.0.6.min', $this->router->class))?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.8.0.min.js"><\/script>')</script>

  <?endif;?>

</head>
<body>

  <header>
    <h1 id="chamada">PROMOÇÃO JP CHENET NA TORCIDA COM VOCÊ - Participe e concorra a 20TVs de LED de 40 polegadas para assistir aos jogos do mundial de 2014 com muito mais emoção!</h1>
    <div class="centro">
      <div id="garrafa"><img src="_imgs/layout/garrafa-destaque.png" alt="JP Chenet"></div>
    </div>
  </header>

  <div class="main">
    <div class="centro checkIdade">
      <div class="box-branco">
         <p class="marrom">Infelizmente, pelas leis do nosso país, você não tem idade para acessar nosso conteúdo.<br> Esperamos você de volta em alguns anos!</p>
      </div>
    </div>
  </div>

  <footer>
    <div class="centro">
      <div class="coluna moderacao">
        <span>BEBA COM MODERAÇÃO.</span>
      </div>
      <div class="coluna info">
        <p>SAC 0800 721 881 &bull; <a href="http://www.lapastina.com" target="_blank" title="La Pastina">www.lapastina.com</a> &bull; Televendas (11)3383-9303</p>
        <p>Certificado de Autorização Caixa n&ordm; 3-1516/2013 - Distribuição Gratuita de Prêmios</p>
      </div>
      <div class="coluna pastina">
        <a href="http://www.lapastina.com" target="_blank" title="La Pastina"><img src="_imgs/layout/la-pastina.png" alt="La Pastina"></a>
      </div>
    </div>
  </footer>


  <?if(ENVIRONMENT != 'development' && GOOGLE_ANALYTICS != FALSE):?>
    <script>
      window._gaq = [['_setAccount','UA<?=GOOGLE_ANALYTICS?>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
  <?endif;?>

  <?JS(array('imagesloaded.pkgd.min' ,'jquery.maskedinput.min', 'fancybox', 'front'))?>

</body>
</html>
