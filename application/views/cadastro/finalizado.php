<div class="main">
	<div class="centro">

		<div class="agradecimento">
			<?php if ($this->session->flashdata('cadastro_finalizado') == 'N'): ?>

				<div class="concluido box-branco">
					Houve um erro ao efetuar o cadastro<br>
					Por favor, tente novamente.
				</div>

				<a href="cadastro" class="link-branco central blocky w70 txtcentral" title="Voltar">VOLTAR</a>

			<?php else: ?>

				<div id="bola"><img src="_imgs/layout/bola.png"></div>

				<div class="concluido box-branco">
					Dados enviados com sucesso.<br>
					Boa sorte!
				</div>

			<?php endif ?>
		</div>
	</div>
</div>