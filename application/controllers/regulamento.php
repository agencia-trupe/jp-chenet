<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Regulamento extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();

   		if($this->session->userdata('maior_de_idade') != 'S')
   			redirect('home');
    }

    function index(){

    	if($this->input->is_ajax_request())
    		$this->hasLayout = FALSE;

   		$this->load->view('regulamento');
    }

}