<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
    	if($this->session->userdata('maior_de_idade') == 'S'){
    		$this->load->view('home');
   		}elseif($this->session->userdata('maior_de_idade') == 'N'){
   			$this->hasLayout = FALSE;
   			$this->load->view('menor');
   		}else{
   			$this->hasLayout = FALSE;
   			$this->load->view('pre-home');
   		}
    }

    function maior(){
    	$this->session->set_userdata('maior_de_idade', 'S');
    	redirect('home/index');
    }

    function menor(){
    	$this->session->set_userdata('maior_de_idade', 'N');
    	redirect('home/index');
    }

}