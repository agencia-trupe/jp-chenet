<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cadastro extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();

   		if($this->session->userdata('maior_de_idade') != 'S')
   			redirect('home');
    }

    function index(){
   		$this->load->view('cadastro/cadastro');
    }

    function enviar(){

    	$msg_erro = "";

		// resposta
    	$resposta = $this->input->post('resposta');
    	if(!$resposta) $msg_erro = "Responda a pergunta para poder participar!";

		// nome
		$nome = $this->input->post('nome');
		if(!$nome && !$msg_erro) $msg_erro = "Informe seu nome!";

		// cpf
		$cpf = $this->input->post('cpf');
		if(!$cpf && !$msg_erro) $msg_erro = "Informe seu cpf!";

		if(!$this->isValid($cpf) && !$msg_erro) $msg_erro = "Informe um cpf válido!";

		// rg
		$rg = $this->input->post('rg');
		if(!$rg && !$msg_erro) $msg_erro = "Informe seu RG!";

		// data_nascimento
		$data_nascimento = $this->input->post('data_nascimento');
		if(!$data_nascimento && !$msg_erro) $msg_erro = "Informe sua data de nascimento!";

		if($this->calculaIdade($data_nascimento) < 18 && !$msg_erro) $msg_erro = "É necessário ser maior de 18 anos para participar!";

		// endereco
		$endereco = $this->input->post('endereco');
		if(!$endereco && !$msg_erro) $msg_erro = "Informe seu endereço!";

		// numero
		$numero = $this->input->post('numero');
		if(!$numero && !$msg_erro) $msg_erro = "Informe o número de seu endereço!";

		// complemento
		$complemento = $this->input->post('complemento');

		// cidade
		$cidade = $this->input->post('cidade');
		if(!$cidade && !$msg_erro) $msg_erro = "Informe sua cidade!";

		// estado
		$estado = $this->input->post('estado');
		if(!$estado && !$msg_erro) $msg_erro = "Informe seu estado!";

		// cep
		$cep = $this->input->post('cep');
		if(!$cep && !$msg_erro) $msg_erro = "Informe seu cep!";

		// telefone
		$telefone = $this->input->post('telefone');
		if(!$telefone && !$msg_erro) $msg_erro = "Informe seu telefone!";

		// email
		$email = $this->input->post('email');
		if(!$email && !$msg_erro) $msg_erro = "Informe seu email!";

		// cupom fiscal
		$cupom_fiscal = $this->input->post('cupom_fiscal');
		if(!$cupom_fiscal && !$msg_erro) $msg_erro = "Informe o número do cupom fiscal!";

		// codigo e barras
		$codigo_barra = $this->input->post('codigo_barra');
		if(!$codigo_barra && !$msg_erro) $msg_erro = "Informe o código de barra do produto!";

		//$check_codigo = $this->db->get_where('cadastros', array('codigo_barra' => $codigo_barra))->num_rows();
		//if($check_codigo > 0 && !$msg_erro) $msg_erro = "Este código de barra já foi cadastrado! $produto1";

		$aceite_regulamento = $this->input->post('aceite_regulamento');
		if($aceite_regulamento == 0 && !$msg_erro) $msg_erro = "Você deve aceitar os termos do regulamento para participar!";

		$receber_newsletter = $this->input->post('receber_newsletter');

		if($msg_erro){
			$this->session->set_flashdata('resposta', $resposta);
			$this->session->set_flashdata('nome', $nome);
			$this->session->set_flashdata('cpf', $cpf);
			$this->session->set_flashdata('rg', $rg);
			$this->session->set_flashdata('data_nascimento', $data_nascimento);
			$this->session->set_flashdata('endereco', $endereco);
			$this->session->set_flashdata('numero', $numero);
			$this->session->set_flashdata('complemento', $complemento);
			$this->session->set_flashdata('cidade', $cidade);
			$this->session->set_flashdata('estado', $estado);
			$this->session->set_flashdata('cep', $cep);
			$this->session->set_flashdata('telefone', $telefone);
			$this->session->set_flashdata('email', $email);
			$this->session->set_flashdata('cupom_fiscal', $cupom_fiscal);
			$this->session->set_flashdata('codigo_barra', $codigo_barra);
			$this->session->set_flashdata('aceite_regulamento', $aceite_regulamento);
			$this->session->set_flashdata('receber_newsletter', $receber_newsletter);
			$this->session->set_flashdata('msg_erro', $msg_erro);
			redirect('cadastro/index', 'refresh');
		}else{
			$insert = $this->db->set('resposta', $resposta)
								->set('nome', $nome)
								->set('cpf', $cpf)
								->set('rg', $rg)
								->set('data_nascimento', formataData($data_nascimento, 'br2mysql'))
								->set('endereco', $endereco)
								->set('numero', $numero)
								->set('complemento', $complemento)
								->set('cidade', $cidade)
								->set('estado', $estado)
								->set('cep', $cep)
								->set('telefone', $telefone)
								->set('email', $email)
								->set('cupom_fiscal', $cupom_fiscal)
								->set('codigo_barra', $codigo_barra)
								->set('aceite_regulamento', $aceite_regulamento)
								->set('receber_newsletter', $receber_newsletter)
								->set('data_cadastro', Date('Y-m-d H:i:s'))
								->set('ip_cadastro', ip())
								->insert('cadastros');

			if($insert){
				$this->session->set_flashdata('cadastro_finalizado', 'S');
				$this->enviarConfirmacao(array(
					'resposta' => $resposta,
					'nome' => $nome,
					'cpf' => $cpf,
					'rg' => $rg,
					'data_nascimento' => $data_nascimento,
					'endereco' => $endereco,
					'numero' => $numero,
					'complemento' => $complemento,
					'cidade' => $cidade,
					'estado' => $estado,
					'cep' => $cep,
					'telefone' => $telefone,
					'email' => $email,
					'cupom_fiscal' => $cupom_fiscal,
					'codigo_barra' => $codigo_barra,
					'receber_newsletter' => ($receber_newsletter == 1) ? 'Sim' : 'Não',
					'data_cadastro' => Date('d/m/Y')
				));
			}else
				$this->session->set_flashdata('cadastro_finalizado', 'N');

			redirect('cadastro/finalizado');
		}
    }

    function finalizado(){
    	$this->load->view('cadastro/finalizado');
    }

    private function isValid($cpf){
		$cpf = str_pad(preg_replace('/[^0-9]/i', '', $cpf), 11, '0', STR_PAD_LEFT);

		if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999'){
			return false;
    	}
		else{
	        for ($t = 9; $t < 11; $t++) {
	            for ($d = 0, $c = 0; $c < $t; $c++) {
	                $d += $cpf{$c} * (($t + 1) - $c);
	            }

	            $d = ((10 * $d) % 11) % 10;

	            if ($cpf{$c} != $d) {
	                return false;
	            }
	        }

        	return true;
    	}
    }

    private function calculaIdade($data_nascimento){
    	if(!$data_nascimento) return '0';

    	$birthDate = $data_nascimento;
        $birthDate = explode("/", $birthDate);
        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y")-$birthDate[2])-1):(date("Y")-$birthDate[2]));
        return $age;
    }

    private function enviarConfirmacao($dados){
        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';
        $emailconf['protocol'] = 'mail';

        //$emailconf['protocol'] = 'smtp';
        //$emailconf['smtp_host'] = '';
        //$emailconf['smtp_user'] = '';
        //$emailconf['smtp_pass'] = '';

        $this->load->library('email');

        $this->email->initialize($emailconf);

        $from = 'campanhas@lapastina.com';
        $fromname = 'J.P. Chenet - Na torcida com você';
        $to = $dados['email'];
        $bcc = 'bruno@trupe.net';
        $assunto = 'Confirmação de Inscrição na promoção J.P. Chenet - Na torcida com você';

        $emailhtml = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Sua inscrição na promoção "J.P. Chenet - Na torcida com você" foi recebida com sucesso.</title>
    <meta charset="utf-8">
</head>
<body>
	<h1>Sua inscrição na promoção "J.P. Chenet - Na torcida com você" foi recebida com sucesso.</h1>
	<h2>Segue a confirmação dos dados cadastrados para você acompanhar e torcer!</h2>
	<p>
		<strong>"EM 2014 QUAL SERÁ SUA COMEMORAÇÃO SE O BRASIL FOR CAMPEÃO?"</strong> : {$dados['resposta']}<br>
		Nome :  {$dados['nome']} <br>
		CPF :  {$dados['cpf']} <br>
		RG :   {$dados['rg']} <br>
		Data de Nascimento :  {$dados['data_nascimento']} <br>
		Endereço :  {$dados['endereco']} <br>
		Número :  {$dados['numero']} <br>
		Complemento :  {$dados['complemento']} <br>
		Cidade :  {$dados['cidade']} <br>
		Estado :  {$dados['estado']} <br>
		CEP :  {$dados['cep']} <br>
		Telefone :  {$dados['telefone']} <br>
		E-mail :  {$dados['email']} <br>
		Cupom Fiscal : {$dados['cupom_fiscal']} <br>
		Código de Barra : {$dados['codigo_barra']} <br>
		Receber novidades e promoções de J.P. Chenet :  {$dados['receber_newsletter']} <br>
		Data de inscrição :  {$dados['data_cadastro']} <br>
	</p>
	<h3>BOA SORTE!</h3>
	<h4>J.P. Chenet</h4>
</body>
</html>
EML;

        $plain = <<<EML
Sua inscrição na promoção "J.P. Chenet - Na torcida com você" foi recebida com sucesso.\r\n
Segue a confirmação dos dados cadastrados para você acompanhar e torcer!\r\n
\r\n
"EM 2014 QUAL SERÁ SUA COMEMORAÇÃO SE O BRASIL FOR CAMPEÃO?" : {$dados['resposta']}\r\n
Nome :  {$dados['nome']} \r\n
CPF :  {$dados['cpf']} \r\n
RG :  {$dados['rg']} \r\n
Data de Nascimento :  {$dados['data_nascimento']} \r\n
Endereço :  {$dados['endereco']} \r\n
Número :  {$dados['numero']} \r\n
Complemento :  {$dados['complemento']} \r\n
Cidade :  {$dados['cidade']} \r\n
Estado :  {$dados['estado']} \r\n
CEP :  {$dados['cep']} \r\n
Telefone :  {$dados['telefone']} \r\n
E-mail :  {$dados['email']} \r\n
Cupom Fiscal : {$dados['cupom_fiscal']}\r\n
Código de Barra : {$dados['codigo_barra']}\r\n
Receber informações e promoções de Divella :  {$dados['receber_newsletter']} \r\n
Data de inscrição :  {$dados['data_cadastro']} \r\n
\r\n
BOA SORTE!\r\n
J.P. Chenet\r\n
EML;

        $this->email->from($from, $fromname);
        $this->email->to($to);

        if($bcc)
            $this->email->bcc($bcc);

        $this->email->reply_to($from);

        $this->email->subject($assunto);
        $this->email->message($emailhtml);
        $this->email->set_alt_message($plain);

        $this->email->send();
    }
}