<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Linguagem extends CI_controller {
    
    function __construct() {
        parent::__construct();
    }

    function index($ling = 'pt') {
        $linguagens = array('pt', 'es', 'en');
        
        if(!in_array($ling, $linguagens)){
            $this->session->set_userdata('language', $ling);
            $this->session->set_userdata('prefixo', $ling.'_');

            if($this->session->userdata('redirect')){
                redirect($this->session->userdata('redirect'));
            }else{
                redirect('home');    
            }
        }else{
            redirect('home');
        }
    }
    
    function jstraduz(){
        echo traduz($this->input->post('string'), TRUE);
    }
    
}

?>